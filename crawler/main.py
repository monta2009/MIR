import httplib2
import json
from BeautifulSoup import BeautifulSoup, SoupStrainer
from collections import deque


MAX_ARTICLES = 3#00
MAIN_SITE_DOMAIN = 'https://www.researchgate.net/'
START_LINK = 'https://www.researchgate.net/researcher/8159937_Zoubin_Ghahramani'
crawler_queue = deque([])

articles = {}

http = httplib2.Http()

def parse(page, article_id):
    global crawler_queue
    global articles
    soup = BeautifulSoup(page)
    links = []
    articles[article_id] = {'name': soup.find('span', {'class': 'publication-title'}).findAll(text=True),
                            'abstract': soup.find('div', {'class': 'pub-abstract'}).findChildren()[0].findChildren()[0].findChildren()[1].findAll(text=True),
                            'authors': [author['content'] for author in soup.findAll('meta', {'name': 'citation_author'})],
                            }

    citation_link = MAIN_SITE_DOMAIN + 'publicliterature.PublicationIncomingCitationsList.html?publicationUid=' + \
                str(article_id) + \
                '&citedInPage=1&swapJournalAndAuthorPositions=0&showAbstract=1&showType=1&showPublicationPreview=1&publicationUid=' + \
                str(article_id) + '&limit=10000000'
    status, response = http.request(citation_link, headers={'accept': 'application/json'})
    json_result = json.loads(response)
    articles[article_id]['citations'] = [c['data']['publicationUid'] for c in json_result['result']['data']['citationItems']]
    links.extend([c['data']['publicationUrl'] for c in  json_result['result']['data']['citationItems']][:10])

    reference_link = 'https://www.researchgate.net/publicliterature.PublicationCitationsList.html?publicationUid=' + \
                str(article_id) + \
                '&showCitationsSorter=true&showAbstract=true&showType=true&showPublicationPreview=true&swapJournalAndAuthorPositions=false&publicationUid=' + \
                str(article_id) + '&limit=10000000'
    status, response = http.request(reference_link, headers={'accept': 'application/json'})
    json_result = json.loads(response)
    articles[article_id]['references'] = [r['data']['publicationUid'] for r in json_result['result']['data']['citationItems']]
    links.extend([r['data']['publicationUrl'] for r in  json_result['result']['data']['citationItems']][:10])

    crawler_queue.extend(links)


if __name__ == '__main__':
    status, response = http.request(START_LINK)
    soup = BeautifulSoup(response)
    crawler_queue.extend([l['href'] for l in soup.findAll('a', {'class': 'js-publication-title-link js-go-to-publication ga-publication-item'})][:5])

    while crawler_queue and len(articles.keys()) <= MAX_ARTICLES:
        link = crawler_queue.popleft()
        status, response = http.request(MAIN_SITE_DOMAIN + link)
        article_id = int(link.split('_')[0].split('/')[-1])
        parse(response, article_id)

