from elasticsearch import Elasticsearch

es = Elasticsearch()

INDEX_NAME = 'MIR'


def create_index():
    if es.indices.exists(index=INDEX_NAME):
        es.indices.delete(index=INDEX_NAME)
    es.indices.create(index=INDEX_NAME)


def index(article_id, article):
    es.index(index=INDEX_NAME, doc_type='article', id=article_id, body=article_id)


def get(article_id):
    return es.get(index=INDEX_NAME, doc_type='article', id=article_id)['_source']


def search(text):
    res = es.search(index=INDEX_NAME, body={
        "query": {
            "multi_match": {
                "query": text,
                "fields": []  # TODO
            }
        }
    })
    return res['hits']['hits']


# create_index()
