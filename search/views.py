from django.views.generic.base import TemplateView


class SearchView(TemplateView):
    template_name = "search.html"

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        return context

    def get(self, request, *args, **kwargs):
        result = super(SearchView, self).get(request, *args, **kwargs)
        return result

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)

        query = request.POST.get('q')
        # result = search(query)

        result = [{
            'id': 1,
            'title': 'SOMETHING',
            'text': 'Dropdowns are automatically positioned via CSS within the normal flow of the document. This means '
                    'dropdowns may be cropped by parents with certain overflow properties or appear out of bounds of th'
                    'e viewport. Address these issues on your own as they arise.Dropdowns are automatically positioned '
                    'via CSS within the normal flow of the document. This means dropdowns may be cropped by parents wit'
                    'h certain overflow properties or appear out of bounds of the viewport. Address these issues on you'
                    'r own as they arise.'
        },
        {
            'id': 2,
            'title': 'asdasdasd',
            'text': 'Dropdowns are automatically positioned via CSS within the normal flow of the document. This means '
                    'dropdowns may be cropped by parents with certain overflow properties or appear out of bounds of th'
                    'e viewport. Address these issues on your own as they arise.Dropdowns are automatically positioned '
                    'via CSS within the normal flow of the document. This means dropdowns may be cropped by parents wit'
                    'h certain overflow properties or appear out of bounds of the viewport. Address these issues on you'
                    'r own as they arise.'
        },
        {
            'id': 3,
            'title': 'tyutyutyut',
            'text': 'Dropdowns are automatically positioned via CSS within the normal flow of the document. This means '
                    'dropdowns may be cropped by parents with certain overflow properties or appear out of bounds of th'
                    'e viewport. Address these issues on your own as they arise.Dropdowns are automatically positioned '
                    'via CSS within the normal flow of the document. This means dropdowns may be cropped by parents wit'
                    'h certain overflow properties or appear out of bounds of the viewport. Address these issues on you'
                    'r own as they arise.'
        }]

        context.update({'search_results': result})

        result = self.render_to_response(context)
        return result
